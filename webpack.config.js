const {join} = require('path')
const {VueLoaderPlugin} = require('vue-loader')
const HtmlPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')

const prod = process.env.NODE_ENV === 'production'

const url_loader = {
    loader: 'url-loader',
    options: {limit: 2048, fallback: 'file-loader?name=[contenthash].[ext]'},
}

const ts_loader = {loader: 'ts-loader', options: {transpileOnly: true}}

module.exports = {
    mode: prod ? 'production' : 'development',
    devtool: prod ? false : 'inline-source-map',

    entry: {
        app: './src/js/app.ts',
    },

    output: {
        path: join(__dirname, './dist'),
        filename: '[name].js?h=[hash]',
        chunkFilename: '[contenthash].js',
        jsonpFunction: 'applyChunk',
    },

    devServer: {
        historyApiFallback: true,
        inline: true,
        hot: true,
    },

    resolve: {
        extensions: ['.js', '.ts'],
    },

    module: {
        rules: [
            {test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/i, use: [url_loader]},
            {test: /\.(jpe?g|png|gif|svg)(\?v=\d+\.\d+\.\d+)?$/i, use: [url_loader, 'img-loader']},

            {test: /\.s?css$/, use: [prod ? MiniCssExtractPlugin.loader : 'style-loader', 'css-loader', 'sass-loader']},

            {test: /\.jsx?$/, loader: 'babel-loader'},
            {test: /\.tsx?$/, use: ['babel-loader', ts_loader]},

            {test: /\.vue$/, loader: 'vue-loader', options: {loaders: {ts: 'ts-loader'}}},

            {
                test: /\.vue\.html$/,
                loader: 'vue-template-loader',
                options: {
                    transformToRequire: {
                        img: 'src',
                    },
                },
            },
        ],
    },

    optimization: {
        splitChunks: {
            chunks: 'all',
            maxInitialRequests: 20,
            maxAsyncRequests: 20,
            minSize: 40,
        },
    },

    plugins: [
        new HtmlPlugin({template: './src/html/index.html', filename: 'index.html', title: 'My Application'}),
        new MiniCssExtractPlugin({filename: '[name].css?h=[hash]', chunkFilename: '[contenthash].css'}),
        new VueLoaderPlugin(),
        new TerserPlugin({sourceMap: !prod, terserOptions: {output: {comments: !prod}}}),
        new CleanWebpackPlugin(),
    ],
}
