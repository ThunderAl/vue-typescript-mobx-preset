import {observable, observe} from 'mobx'
import TextModel from './TextModel'

class TestModel {
    @observable changes: number = 0
    @observable text: string = ''
    @observable history: TextModel[] = []

    constructor() {
        observe(this, 'text', change => {
            this.changes++
            if (change.oldValue)
                this.history.push(new TextModel(change.oldValue))
        })
    }
}

export default new TestModel()
