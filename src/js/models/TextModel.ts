import {observable} from 'mobx'

export default class TextModel {
    @observable text: string = ''

    constructor(text: string) {
        this.text = text
    }
}
