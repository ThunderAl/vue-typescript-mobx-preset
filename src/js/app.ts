import VueNode from './VueNode'

export const el = document.createElement('div')
export const vue_node = VueNode
document.body.prepend(el)
vue_node.$mount(el)
