import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const index = new VueRouter({
    linkExactActiveClass: 'active',
    mode: 'history',
    routes: [
        {path: '', component: () => import('../pages/StartPage/StartPage.vue'), name: 'start'},
        {path: '/another', component: () => import('../pages/AnotherPage/AnotherPage.vue'), name: 'another-page'},
        {path: '/mobx', component: () => import('../pages/MobxTest/MobxTest.vue'), name: 'mobx-test'},
        {path: '*', redirect: {name: 'start'}},
    ],
})

export default index

