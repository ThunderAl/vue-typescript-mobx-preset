import Vue from 'vue'
import router from './router'
import VueApp from './components/VueApp.vue'

Vue.config.productionTip = false

export default new Vue({
    router,
    render: r => r(VueApp),
})
